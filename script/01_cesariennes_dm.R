###################SHINY DASHBOARD - OBSTERTRICAL###############################


# December 2022

# Datamanagement de fichier de recensement de l'activité d'un service de maternité
# Modification des données présentée par ajout d'un coefficient de 1,03342 
# (score ARG-FR WC Quatar 22) d'anonymisation des données


# Mise à jour en MAI 2023 :
# Séparation des scripts par fichier
##############################SESSION INFOS#####################################

## RStudio 2022.12.0+353 "Elsbeth Geranium" Release (2022-12-03) pour macOS
## macOS Ventura 13.1 (22C65)
## git https://gitlab.com/antoine_teston/shiny_obstetrical_activity
## published on https://antoineteston.shinyapps.io/shinydashboard_obstetrical_activity/

##############################PACKAGES##########################################
library(dplyr)
library(tidyr)
library(readr)
library(stringr)
library(readxl)
library(purrr)

################################Function file###################################

function_src = ("./00_functions.R")
source(file = function_src)

###########################DATA MANAGEMENT######################################

#######################cesariennes.xlsx#########################################

## Sources
cesar = readxl::read_xlsx("../data/cesariennes.xlsx")

## Formatage noms de colonnes
colnames(cesar) = tolower(colnames(cesar))
colnames(cesar) = gsub(" ", "_", colnames(cesar))
cesar$id = seq_len(nrow(cesar))

## Récupération des données nécessaires
cesar_filter = cesar %>%
  select(id, total_cesariennes, acte_principal, urgence)

## Formatage de la colone d'acte_principal
cesar_acte_principal = cesar_filter %>%
  select(id, acte_principal)

## Appel de fonction match_column pour effectuer un grepl en fonction des termes dans le texte
cesar_acte_principal = cesar_acte_principal %>%
  match_column(cesar_acte_principal$acte_principal, "CESARIENNE", "cesarienne") %>%
  match_column(cesar_acte_principal$acte_principal, "SIMPLE", "simple") %>%
  match_column(cesar_acte_principal$acte_principal, "EN COURS DE TRAVAIL", "en_cours_de_travail") %>%
  match_column(cesar_acte_principal$acte_principal, "AVANT TRAVAIL", "avant_travail") %>%
  match_column(cesar_acte_principal$acte_principal, "MULTI-CICATRICIEL", "multi_cicatriciel") %>%
  match_column(cesar_acte_principal$acte_principal, "VERT", "code_vert") %>%
  match_column(cesar_acte_principal$acte_principal, "ORANGE", "code_orange") %>%
  match_column(cesar_acte_principal$acte_principal, "ROUGE", "code_rouge") %>%
  match_column(cesar_acte_principal$acte_principal, "REPRISE", "reprise_chir")

## Merge de la DF finale

cesar_final <- merge(cesar_filter, cesar_acte_principal, by = c("id", "acte_principal"))
cesar_final <- arrange(cesar_final, id)

## Listing variables possibles
nb_cesar = sum(cesar_final$cesarienne == "TRUE")
nb_simple = sum(cesar_final$simple == "TRUE")
nb_encours_W = sum(cesar_final$en_cours_de_travail == "TRUE")
nb_avantW = sum(cesar_final$avant_travail == "TRUE")
nb_multicica = sum(cesar_final$multi_cicatriciel == "TRUE")
nb_urgence = sum(cesar_final$urgence == "Oui")
nb_reprise = sum(cesar_final$reprise_chir == "TRUE")
code_vert = sum(cesar_final$code_vert == "TRUE")
code_orange = sum(cesar_final$code_orange == "TRUE")
code_rouge = sum(cesar_final$code_rouge == "TRUE")

#### Urgence ####

## Création d'un dataframe d'export de prise en charge selon gravité, avec nb d'effectif selon le code couleur
## d'urgence

DF_urg = data.frame(pop = c("Code_vert","Code_orange","Code_Rouge","NR"), 
                    nb = c(code_vert,code_orange,code_rouge,
                           (nb_urgence - (code_vert+code_orange+code_rouge))))

# Niveau de facteur d'ordre, pour traitement des plots
DF_urg$pop <- factor(DF_urg$pop, levels = c("Code_vert","Code_orange","Code_Rouge","NR"))

## Indicateur d'export RDATA (possibilité d'utilisation pour une valuebox du shinydashboard)
indic_urg = round((code_rouge / nb_urgence),2) #PEC code rouge
indic_urg_tot = round(nb_urgence / tail(cesar_final$id, 1),2) #Urgence / total césar
indic_reprise = round(nb_reprise / tail(cesar_final$id, 1),2) #Urgence / Reprise chirurgicale

## Préparation graphe des césariennes en cours de travail (code urgence)
## Avec duplication dataframe principale et application d'un filtre sur les césariennes en cours de travail
urg_coursdeW <- cesar_final %>%
  filter(cesar_final$en_cours_de_travail == "TRUE")

## Application fonction de création/compte effectif code d'urgence automatique
DF_urg_en_cours <- count_codes(urg_coursdeW) 
## <- export pour plot prise en charge par code des césarienne en cours de travail

## Préparation graphe césarienne en cours de W VS Prog (avant travail, simple, multi cat)
# Césarienne en cours de travail déjà créé
type_urg_en_cours <- count_types(urg_coursdeW)
## Retourne 0 / donc non exploitable....

## Préparation graphe des césariennes programmées avec duplication dataframe d'origine et application
## d'un filtre sur les urgences. Puis mise en fonction d'une variante de count_codes mais avec les spécifités
## des césariennes (Simple, avant travail ou multi-cicatriciel)
cesar_prog <- cesar_final %>%
  filter(cesar_final$urgence == "Non")
type_cesar_prog <- count_types(cesar_prog) #. <- export pour plot

## Exportation des données de césariennes : 

save(indic_urg,indic_urg_tot,indic_reprise,DF_urg,DF_urg_en_cours,type_cesar_prog, file = "../script/RData/cesariennes.RData")