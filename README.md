# Shiny Obstetrical Activity

-- 

## Objectifs :

1. Développement d'un dashboard de reporting à partir de 2 datasets d'historique d'activité d'un service d'obstétrique : 
	* Le suivi des césariennes
	* L'activité clinique depuis 2005 jusque 2021.

1. Développer un thème/objectif/sujet au dashboard en relation avec l'activité du service et respectant les points ci-dessous:
	* \+ 2 graphiques
	* Indicateurs
	* \+ 2 panels 
	* Filtres intéractifs
	* Techno par Shiny dashboard

## Déroulement

* Data-management en deux parties (entrecoupé par l'exportation de 15 CSV représentant les datas du fichier de 2005 à 2020). Le traitement du fichier des data de 2021 à été effectué avec le fichier complet.

* Utilisation d'un fichier functions.R d'algos de DM.

* Anonymisation par coefficient multiplicateur (pour présentation)

* Le Dashboard a été développé sur Shiny à l'aide de la library *shinydashboard*

## Capture d'écran : 

![](../png/02.png)

![](../png/337507692_164250816524232_912793269827425845_n.png)


## Rédaction / méthodo article

Après MAJ des recherches en incluant le « visual analytics » et en affinant mieux les lectures, tu trouveras en PJ le nouveau Zotero. J’y ai inclus quelques commentaires sur chaque article.

Je me permet de conseiller tout de même les lectures suivantes : 

Pour le développement d’indicateurs à des fins d’implémentation dans des dashboards : 
	- A Novel early pregnancy assessment unit/Gynecology assessment unit dashboard: An experience from a UK district general hospital - 2015
	- Development of maternity dashboards across a UK health region; current practice, continuing problems - 2013
	- Development and Implementation of Maternity Dashboard in Regional Hospital for Quality Improvement at Ground Level: A Pilot Study - 2018 en Oman (plus récent)

Sur un aspect un peu plus visuel : 
	- Adaptation and implementation of local maternity dashboards in a Zimbabwean hospital to drive clinical improvement - 2014
	- A Novel early pregnancy assessment unit/Gynaecology assessment unit dashboard: An experience from a UK district general hospital - 2015

1) Intro

[Pour le pilotage, on a besoin d'indicateurs, retours, outils, ...
Description data visualisation. Ce qu'il existe en obstétrique.
Ce qu'on va faire.]

Le développement d'outils de visualisation à des fins d'analyses sont des choses habituelles dans des secteurs financiers et économiques. Ils permettent, via des représentations graphiques, indicateurs ou de dashboards, d'apprécier une activité que ce soit quantitativement, qualitativement ou les deux. Le développement de ces outils dans le secteur médico-social est exponentiel avec le développement de la data science dans ces secteurs.  


2) Méthode

2.1) End users

Qui va l'utiliser ? => 

Tableur excel brut envoyé une fois par an à la clinique d'obstétrique. Améliorer sur la présentation et la fréquence.
Tous les soignants de l'équipe : Obstétriciens, sage-femmes, puéricultrice, cadre de santé, direction.

A quelle fréquence ? Tous les 3/6 mois.

Pourquoi ?
Pilotage Rapport d'activité, nombre de naissance attendu, si on augmente le nombre de césarienne, plus de soin
Soins : QUalité des soins.
Recherche pour savoir -15% d'hémoragie, pourquoi ? 

Dans quel contexte ? Au bloc, au bureau, en réunion.
Afficher en salle de naissance / repos, réunion de service, 
Rapider d'accès au cherche.

1) Accouchement Activité
voies instrumentés, voies basses

2) Année en cours

3) Toutes les années

4) Césariennes

Codes couleurs, en cours de travail. 

SA en %

Quels indicateurs pour répondre aux questions ?
Quel niveau de détail ? temps (mois, année)
Quels filtres ?
Quelles fonctionnalités ? Impression. Export de tableaux et de graphiques.

2.2) Synthèse

On a sélectionné des thèmes et des indicateurs.
Cahier des charges : thèmes (combien de tableaux), indicateurs graphiques et numérique, tableaux, fonctionnalités.

2.3) Prototypage :

Pour un indicateur, proposer plusieurs représentations.
Présenter plusieurs graphiques côte à côté. Lequel on choisit : classement pour chaque représentation.


2.4) Déploiement

2.5) Evaluation

On pose des questions, les utilisateurs doivent retrouver les chiffres sur le tableau de bord.

Q1: I think that I would like to use this system frequently
Q2: I found the system unnecessarily complex
Q3: I thought the system was easy to use
Q4: I think that I would need the support of a technical person to be able to use this system
Q5: I found the various functions in this system were well integrated
Q6: I thought there was too much inconsistency in this system
Q7: I would imagine that most people would learn to use this system very quickly
Q8: I found the system very cumbersome to use
Q9: I felt very confident using the system
Q10: I needed to learn a lot of things before I could get going with this system

3) Résultats

3.1) End users

Quelles personnes interviewées (profil) ?

3.2) Synthèse 

Résumé des besoins et des spécifications

3.3) illustrations

Installation et imprim écran

3.4) Evaluation

Score SUS

4.) Discussion

Dans les changements, passer en pourcentage.


----------------------------------

- MAJ 06/06 ; modification en proportion, aspects quelques plots, début de prévision d'automatisation